const words = [
    'フルスピ',
    '走るのが俺の人生',
    '俺の人生だった'
];

let showedRemoveComment = 0;

const sendLog = (message, object = null) => {
    console.info(
        `%cYoutube Comment Blocker%c ${message}`,
        'color:#fafafa; background:#c62828; padding:2px; border-radius: 3px; font-weight:bold; font-size: 11px', 'font-size:12px',
        object);
}

const removeTargetTextComment = (text) => {
    let removeCount = 0;
    document.querySelectorAll('.ytd-comments #contents .ytd-item-section-renderer').forEach(commentThreadElement => {
        const commentElement = commentThreadElement.querySelector('yt-formatted-string#content-text.ytd-comment-renderer');
        if (commentElement === null) {
            return;
        }
        if (commentElement.textContent.indexOf(text) !== -1) {
            commentElement.textContent = '';
            commentThreadElement.style.display = 'none';
            removeCount++;
            showedRemoveComment++;
        }
    });
    return removeCount;
}

const removeTargetCommentFromWordList = () => {
    const blockInfo = [];
    words.forEach(word => {
        const removeCount = removeTargetTextComment(word);
        if (removeCount > 0) {
            blockInfo.push({
                word,
                removeCount
            })
        }
    });
    if (blockInfo.length > 0) {
        sendLog(`設定されている単語を含むコメントを非表示にしました`, blockInfo);
        document.querySelector('.ycbext-notify-area').textContent = `設定されている単語を含むコメントを${showedRemoveComment}件非表示にしました`;
        if (!document.querySelector('.ycbext-notify-area').classList.contains('open')) {
            document.querySelector('.ycbext-notify-area').classList.add('open');
            setTimeout(() => {
                document.querySelector('.ycbext-notify-area').classList.remove('open');
                showedRemoveComment = 0;
            }, 10000);
        }
    }
}

window.addEventListener('load', () => {
    document.body.insertAdjacentHTML('afterbegin', '<div class="ycbext-notify-area">設定されている単語を含むコメントを非表示にしました</div>');

    setInterval(() => {
        removeTargetCommentFromWordList();
    }, 1000);

    chrome.runtime.onMessage.addListener((message, sender, callback) => {
        switch (message.type) {
            case 'webRequest':
                if (message.url.indexOf('https://www.youtube.com/youtubei/v1/next') !== -1 && message.event === 'onCompleted') {
                    setTimeout(() => {
                        removeTargetCommentFromWordList();
                    }, 2000);
                }
                break;
            case 'tabs':
                if (message.url.indexOf('https://www.youtube.com/watch') !== -1 && message.event === 'onUpdated') {
                }
                break;
            default:
                break;
        }
        callback();
        return true;
    });
});
