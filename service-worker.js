const sendLog = (title, message, object = null) => {
    console.info(
        `%c${title}%c ${message}`,
        'color:#fafafa; background:#1b5e20; padding:2px; border-radius: 3px; font-weight:bold; font-size: 11px', 'font-size:12px',
        object !== null ? object : '');
}

const genStrSeed = (count) => {
    const chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
    return Array.from(Array(count)).map(() => chars[Math.floor(Math.random() * chars.length)]).join('');
}

const wait = (callback, retryCount = 1, id = null) => {
    id = id === null ? genStrSeed(8) : id;
    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, tabs => {
        if (tabs.length === 0 || tabs[0].status !== 'complete') {
            sendLog('Callback Delay', `Callback Retry...${retryCount}⏳(id: ${id})`);
            setTimeout(() => {
                wait(callback, retryCount + 1, id);
            }, 1000);
        } else {
            sendLog('Callback Delay', `Callback Execute😸(Total Retry: ${retryCount - 1}, id: ${id})`);
            callback(tabs[0]);
        }
    });
}


chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
    if (tab.url.indexOf('https://www.youtube.com') === -1) {
        return;
    }
    if (changeInfo.status !== 'complete') {
        return;
    }
    sendLog('tabs', 'onUpdated🐈', tab);

    if (tab.url) {
        wait(currentTab => {
            chrome.tabs.sendMessage(tabId, {
                type: 'tabs',
                event: 'onUpdated',
                url: tab.url,
            }, null);
        });
    }
});

chrome.webRequest.onCompleted.addListener(details => {
    if (details.url.indexOf('https://www.youtube.com') === -1) {
        return;
    }
    if (details.url.indexOf('https://www.youtube.com/youtubei/v1/log_event') !== -1) {
        return;
    }
    sendLog('webRequest', 'onComplete🐈', details);

    if (details.url && details.tabId > 0) {
        wait(currentTab => {
            chrome.tabs.sendMessage(currentTab.id, {
                type: 'webRequest',
                event: 'onCompleted',
                url: details.url,
            }, null);
        })
    }
}, { urls: ['https://www.youtube.com/*'] }, []);